The ``raspi_with_qt5`` sets up a Raspberry Pi to have our custom build of Qt5.
After you have run the other playbooks you can use *this* playbook to deploy
Qt5 to an entire fleet of Raspberry Pies.