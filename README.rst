################################################
Cross-compiling Qt for Raspberry Pi with Ansible
################################################

Setting up Ubuntu 18.04 for cross-compiling Qt 5 software is complicated, so here are some Ansible playbooks to make it less painful.

These playbooks were tested with Ansible 2.5.1 on a Linux desktop running Ubuntu 18.04.2 LTS and the Pi running a clean install of the Pi Foundation's Raspbian Buster Lite, release 2018-07-10.  These playbooks are based on the excellent wiki guide at https://wiki.qt.io/RaspberryPi2EGLFS.


************
Requirements
************


1. Software Setup on Host
=========================

You'll need to install Ansible and OpenSSH on your Ubuntu desktop::

   sudo apt install ansible openssh-server
   sudo systemctl enable ssh
   sudo systemctl start ssh


2. Set up Your Hosts File
=========================

#) Get the IP address of the Pi.

#) On Ubuntu, add an entry to ``/etc/hosts`` with the Pi's IP address and hostname, for example::

    192.168.1.3 pi


3. SSH setup
============

You should be able to SSH into both your Ubuntu system and your Pi.  To automate the login process, create a SSH key (when prompted, accept all the defaults)::

    ssh-keygen -b 2048 -t rsa &&
    cat ~/.ssh/id_rsa.pub >> ~/.ssh/authorized_keys &&
    chmod go-rw ~/.ssh/authorized_keys &&
    cat ~/.ssh/id_rsa.pub | ssh pi@pi "mkdir -p ~/.ssh && cat >> ~/.ssh/authorized_keys && chmod go-rwx ~/.ssh/authorized_keys ~/.ssh"



*************
The Playbooks
*************

The playbooks are listed here in the order that they should be run.

**NOTE** All playbooks should be run with the --ask-become option so that you can supply the sudo password.


01_initial_host_setup.yml
=========================

Sets up the host system to support local development.  Installs whatever version of GCC, CMake, etc. that the distribution provides, then installs the version of Qt listed in the ``vars/vars.yml`` file.  This includes Qt Creator, so at the end of this playbook you'll be able to build and run amd64 apps on the host machine.

Running the playbook::

  ansible-playbook 01_initial_host_setup.yml --ask-become -i inventory.txt


02_base_pi_updates.yml
======================

This playbook sets up a Pi.  The command will look like::

    ansible-playbook 02_base_pi_updates.yml --ask-become -i inventory.txt

Playbook highlights include:

* Enabling Raspbian source repos.

* Installing dependencies required for building Qt.

**This playbook will reboot the Pi if the kernel/firmware get updated**


03_setup_cross_compilation.yml
==============================

This playbook sets up the actual cross-compilation toolchain on the Ubuntu host, which includes compiling the ARM version of Qt that you'll later deploy to the Pi.  There's a few things you have to do before running this playbook:

#) Make sure the Pi is online and accessible from the Ubuntu host via the ``pi`` hostname.  Use ``ssh pi`` to confirm everything is working.  The Pi must be accessible because this playbook will copy files from the Pi to the Ubuntu host.

#) Make sure you've already run the Pi setup playbook (``01_pi_setup_playbook.yml``), or you'll be missing required files.

#) Run this playbook with the ``--ask-become`` option so that you can supply Ansible with your sudo password.  The command line will look like::

    ansible-playbook 03_setup_cross_compilation.yml --ask-become -i inventory.txt

**More Details**: Highlights from this playbook include:

* Installing required packages.
* Creating a ~/raspi folder to contain the cross-compilation software.
  This includes copying required files from an actual Pi, which is why a
  Pi must be accessible from the dev computer.
* Compiling Qt 5.12 for the Raspberry Pi.


04_deploy_qt_to_pi.yml
======================

This playbook deploys Qt to the Pi.  You can customize it to deploy your software to the Pi too.

The command line will look like::

    ansible-playbook 04_deploy_qt_to_pi.yml --ask-become -i inventory.txt


**NOTE** This playbook can be used to deploy your Qt build to a whole fleet of Raspberry Pis, not just the Pi you've been using for development.  The playbook
includes required tasks such as updating the firmware, symlinking the correct
OpenGL libraries, and so on.


****
Test
****

After the playbooks have successfully run we can try to cross-compile one of Qt's sample applications.  Assuming you used ~/rpi as the ``pi_folder`` in the Ansible playbooks, run the following commands::

    cd ~/crosscompile/5.12.3/qt-everywhere-src-5.12.3/qtquickcontrols2/examples/ &&
    ~/crosscompile/5.12.3/qt5/bin/qmake &&
    make &&
    scp quickcontrols2/gallery/gallery pi:/home/pi

Now log in to your Pi and run the /home/pi/gallery.  You should be presented with the Qt QuickControls 2 demo gallery.



***********************
Building Your Own Stuff
***********************

Assuming you're using a QMake .PRO project, change to your project's directory and run::

    ~/crosscompile/5.12.3/qt5/bin/qmake

to generate the correct makefile, then you can build your project with ``make``.



****
More
****
I posted a little more about this at `my website <https://woltman.com/blog/cross-compile-qt-for-raspberrypi-on-ubuntu/>`_, including a flow chart.
